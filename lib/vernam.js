vernam = {
 u8FromStr:
  function u8fromStr( str )
  {
  	try
  	{
    return unescape( encodeURIComponent( str ) );
   }
   catch( e )
   {
   	 return '';
   }
  }
  ,
 u8ToStr:
  function( u8str )
  {
  	try
  	{
  		return decodeURIComponent( escape( u8str ) );
  	}
   catch( e )
   {
   	 return '';
   }
  }
  ,
 u8FromBase64:
  function ( str )
  {
  	try
  	{
  	 return atob( str );
  	}
   catch( e )
   {
   	 return '';
   }
  }
  ,
 u8ToBase64:
  function ( u8str )
  {
  	try
  	{
  	return btoa( u8str );
  	}
   catch( e )
   {
   	 return '';
   }
  }
  ,
 genRandom:
  function( size )
  {
  	var buffer = new Uint8Array( size );
   window.crypto.getRandomValues( buffer );
   var u8 = '';
   for( var i = 0; i < size; ++i )
   {
     u8 += String.fromCharCode( buffer[ i ] );
   }
   return u8;
  }
  ,
 splitKey:
  function( key )
  {
  	var half = key.length / 2;
  	
  	return [
  	 { 
     value: key.substr( 0, half )
    }
    ,
    {
     value: key.substr( half, half )
    }
   ];
  }
  ,
 keyReader:
  function( key )
  {
  	return {
  		data: key,
  		offset: 0,
  		last: key.value.length,
  		read: function( size )
  		{
  			if( this.offset + size > this.data.value.length )
  			{
  				this.offset = this.data.value.length;
  				return '';
  			}
  			this.offset += size;
  			return this.data.value.substr( this.offset - size, size );
  		}
  		,
  		save: function()
  		{
  			this.last = this.offset;
  		}
  		,
  		restore: function()
  		{
  			this.offset = this.last;
  			this.clear();
  		}
  		,
  		clear: function()
  		{
  			this.last = this.data.value.length;
  		}
  	}
  }
  ,
 addChecksum:
  function( text )
  {
   	 var res = 0;
  	 for( var i = 0; i < text.length; ++i )
  	 {
  	 	 var ch = text.charCodeAt( i );
  	 	 if( ch >= 256 )
  	 	 {
  	 	 	 return '';
  	 	 }
  	 	 res = res ^ ch;
  	 }
  	 return text + String.fromCharCode( res ) + '\u00ff';
  }
  ,
 removeChecksum:
  function( text )
  {
  	if( text.length < 2 )
  	{
  		return '';
  	}
  	var source = text.substr( 0, text.length - 2 );
  	var expected = vernam.addChecksum( source );
  	 
  	if( expected != text )
  	{
  	 return '';
  	}
 	 return source;
  }
  ,
 getCryptMaterial:
  function( reader, textLength, decrypt )
  {      	
   var result = {
  	 pattern: '',
  	 data: ''
    };
   if( !textLength )
   {
   	 return;
   }
   while( textLength > 0 )
   {
  	 var pattern = reader.read( 1 );
  	 if( pattern == '' )
  	 {
  	 	 return;
  	 }
  	 
  	 pattern = pattern.charCodeAt( 0 );
  	 
  	 var data = reader.read( 8 );
  	 if( data.length != 8 )
  	 {
  	 	 return;
  	 }
  	 
  	 result.data += data;
  	 
  	 var tempPattern = '';
  	 for( var i = 0; i < 8; ++i )
    {
     var isUsed = ( ( pattern >> i ) & 1 );
     tempPattern = ( '' + isUsed ) + tempPattern;
    
     if( 1 == isUsed || decrypt )
     {
     --textLength;
     }
    }
    result.pattern += tempPattern;
   }  
   return result;
  }
  ,
 encrypt:
  function( text, reader )
  {
  	text = this.addChecksum( text );
  	if( !text.length )
  	{
  		return '';
  	}
   var material = this.getCryptMaterial( 
    reader, text.length, false );
   if( !material )
   {
   	 return '';
   }
   var result = '';
   var textOffset = 0;
   for( var i = 0; i < material.data.length; ++i )
   {
    if( '0' == material.pattern[ i ] || textOffset >= text.length )
    {
     result += material.data[ i ];
    }
    else
    {
     result += String.fromCharCode( text.charCodeAt( textOffset ) ^ material.data.charCodeAt( i ) );
     ++textOffset;
    }
   }
   return result;
  }
  ,
 decrypt:
  function( text, reader )
  {
   var material = this.getCryptMaterial( reader, text.length, true );
   if( !material )
   {
   	 return '';
   }
   var result = '';
   var temp = '';
   for( var i = 0; i < material.data.length; ++i )
   {
    if( '0' == material.pattern[ i ] )
    {
     if( text.charCodeAt( i ) != material.data.charCodeAt( i ) )
     {
      return '';
     }
    }
    else
    {
     var res = ( text.charCodeAt( i ) ^ material.data.charCodeAt( i ) );
     if( res )
     {
       result += temp;
       temp = '';
    	  result += String.fromCharCode( res );
    	}
     else
     {
     	temp += String.fromCharCode( res );
     }
    }
   }
   result = this.removeChecksum( result );
   if( result == '' )
   {
   	 return '';
   }
   return result;
  }
  ,
 loadKey:
  function( keyBase64, myKeyId )
  {
  	if( myKeyId !== 0 && myKeyId !== 1 )
  	{
  	 return;
  	}
  	var key = this.u8FromBase64( keyBase64 );
  	
  	if( key.length === 0 )
  	{
  	 return;
  	}
  	
  	var splited = this.splitKey( key );
  	return {
  		my: this.keyReader( splited[ myKeyId ] ),
  		their: this.keyReader( splited[ 1 - myKeyId ] ),
  		id: myKeyId
  	};
  }
};